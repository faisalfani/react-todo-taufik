import logo from "./logo.svg";
import "./App.css";
import { Link, Route, Switch, useParams } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Home from "./Home";

const useStyles = makeStyles({
  root: {
    width: 500,
  },
});
function App() {
  const [state, setState] = useState();
  return (
    <div className="App">
      <Box>asdfsF</Box>
      <Button variant="outlined" color="secondary">
        Default
      </Button>

      <div>
        <nav>
          <ul style={{ listStyle: "none" }}>
            <li style={{ display: "inline", marginLeft: "10px" }}>
              <Link to="/">Home</Link>
            </li>
            <li style={{ display: "inline", marginLeft: "10px" }}>
              <Link to="/about/nama/faisal/id/23">About</Link>
            </li>
            <li style={{ display: "inline", marginLeft: "10px" }}>
              <Link to="/users">Users</Link>
            </li>

            <Link to="/users">
              <button
                style={{
                  backgroundColor: "salmon",
                  border: "solid black 1px",
                  borderRadius: "10px",
                  padding: "8px 12px",
                }}
              >
                tambah user
              </button>
            </Link>
          </ul>
        </nav>
      </div>
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/about/nama/:nama/id/:id">
          <About />
        </Route>
        <Route path="/users">
          <Users />
        </Route>
      </Switch>
    </div>
  );
}

function About() {
  const { nama, id } = useParams();
  return (
    <h2>
      About {nama} : {id}
    </h2>
  );
}

function Users() {
  return <h2>Users</h2>;
}

export default App;
